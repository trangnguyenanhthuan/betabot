---
layout: post
title:  "Bài 1: Các khái niệm cơ bản"
toc: true
tags: ps
---

#### I. Phép thử, biến cố và không gian mẫu

##### 1. Phép thử (Probabilistic experiment)

**Định nghĩa:**

Việc thực hiện một nhóm các điều kiện cơ bản để quan sát một hiện tượng nào đó có xảy ra hay không được gọi là phép thử.

**Ví dụ:**

Tung đồng xu là một phép thử.


##### 2. Biến cố(Event)

**Định nghĩa:**

Hiện tượng có thể xảy ra hoặc không thể xảy ra trong kết quả của phép thử được gọi là biến cố.

**Các loại biến cố:**

- Biến cố chắc chắn: là biến cố chắc chắn xảy ra khi thực hiện phép thử. Ký hiệu $$\Omega$$.

- Biến cố không thể có: là biến cố không thể xảy ra khi thực hiện phép thử. Ký hiệu $$\varnothing$$.

- Biến cố ngẫu nhiên: là biến cố có thể xảy ra hoặc không thể xảy ra khi thực hiện phép thử. Ký hiệu $$A, B, C, ...$$

**Ví dụ:**

Thực hiện phép thử tung một con xúc xắc:

- Biến cố "xuất hiện mặt có số chấm từ 1 đến 6" là biến cố chắc chắn.

- Biến cố "xuất hiện mặt có số chấm bằng 7" là biến cố không thể có.

- Biến cố "xuất hiện mặt 5 chấm" là biến cố ngẫu nhiên. 


##### 3. Không gian mẫu(Sample Space)

**Định nghĩa:**

Không gian mẫu của một thí nghiệm hay của một phép thử ngẫu nhiên là tập hợp của tất cả các kết quả có thể xảy ra của thí nghiệm hay phép thử đó. 

**Ký hiệu:**

$$S$$ hoặc $$\Omega$$.

**Ví dụ:**

Thực hiện phép thử tung một đồng xu, thì không gian mẫu là tập hợp {ngửa, sấp}.


#### II. Tập hợp


##### 1. Các phép toán tập hợp (Set operations)


###### a. Phép hợp (Union)

**Khái niệm:** 

Cho $$A$$ và $$B$$ là hai biến cố bất kì, hợp của $$A$$ và $$B$$, là biến cố chứa tất cả các kết quả hoặc chỉ thuộc về $$A$$, hoặc chỉ thuộc về $$B$$, hoặc thuộc về cả $$A$$ lẫn $$B$$. Do vậy, biến cố này xảy ra khi có ít nhất $$A$$ hoặc $$B$$ xảy ra.

![alt text]({{ "/assets/imgs/ps/b1/1.png" | relative_url }})

<p class="text-center">1. Phép hợp</p>

**Ký hiệu:**

$$A \cup B$$ hoặc $$A + B$$

**Ví dụ:**

- $$A$$ là biến cố: "Tung xúc xắc ra được mặt 1 chấm"

- $$B$$ là biến cố: "Tung xúc xắc ra được mặt 2 chấm"

- $$A \cup B$$ là biến cố: "Tung xúc xắc ra được mặt 1 chấm hoặc 2 chấm"

###### b. Phép giao (Intersection)

**Khái niệm:** 

Cho A và B là hai biến cố bất kì, giao của A và B, là biến cố chứa tất cả các kết quả cùng thuộc về cả A lẫn B. Do vậy, biến cố này xảy ra khi cả A và B cùng xảy ra.

![alt text]({{ "/assets/imgs/ps/b1/2.png" | relative_url }})

<p class="text-center">Hình 2. Phép giao</p>

**Ký hiệu:**

$$A \cap B$$ hoặc $$A.B$$

**Ví dụ:**

- $$A$$ là biến cố: "Tung xúc xắc ra được mặt chẵn chấm".

- $$B$$ là biến cố: "Tung xúc xắc ra được mặt nhỏ hơn 3 chấm".

- $$A \cap B$$ là biến cố: "Tung xúc xắc ra được mặt 2 chấm".

###### c. Phép bù (Intersection)

**Khái niệm:** 

Cho A là biến cố bất kì, bù của A là biến cố chứa tất cả biến cố trong không gian mẫu S nhưng không thuộc về A.

![alt text]({{ "/assets/imgs/ps/b1/3.png" | relative_url }})

<p class="text-center">Hình 3. Phép bù</p>

**Ký hiệu:**

$$\bar{A}$$ hoặc $$A^c$$

**Ví dụ:**

- $$A$$ là biến cố: "Tung xúc xắc ra được mặt 1 chấm".

- $$\bar{A}$$ là biến cố: "Tung xúc xắc ra được mặt lớn hơn 1 chấm".

##### 2. Tính chất

| Tên | Tính chất |
|-----------|---------------------------------------------------------------------------------------------------------------------------|
| Giao hoán | $$A \cup B = B \cup A$$<br>$$A \cap B = B \cap A$$ |
| Kết hợp | $$A \cup (B \cup C) = (A \cup B) \cup C$$<br>$$A \cap (B \cap C) = (A \cap B) \cap C$$ |
| Phân phối | $$A \cap (B \cup C) = (A \cap B) \cup (A \cap C)$$<br>$$A \cup (B \cap C) = (A \cup B) \cap (A \cup C)$$ |
| Phần bù | $$\bar{\bar{A}} = A$$<br>$$\overline{A \cup B} = \bar{A} \cap \bar{B}$$<br>$$\overline{A \cap B} = \bar{A} \cup \bar{B}$$ |

<p class="text-center">Bảng 1. Bảng tính chất của tập hợp</p>

#### III. Hoán vị, Tổ hợp và Chỉnh hợp.

##### 1. Các phép đếm

###### a. Quy tắc cộng:

**Khái niệm:**

Giả sử một công việc có thể được thực hiện theo phương án A hoặc phương án B. Có n cách thực hiện phương án A, m cách thực hiện phương án B. Khi đó công việc có thể được thực hiện bởi m+n cách.

**Ví dụ:**

Tung 2 con xúc xắc được số chấm có tổng bằng 7 hoặc 8.

Có 2 trường hợp:

- Tổng số chấm bằng 7 có 3 trường hợp : 1+6, 2+5, 4+3.
- Tổng số chấm bằng 8 có 3 trường hợp: 2+6, 3+5,4+4.

⇒ Vậy có 6(=3+3) trường hợp thỏa mãn biến cố trên.


###### b. Quy tắc nhân:

**Khái niệm:**

Giả sử một công việc gồm 2 công đoạn A và công đoạn B.  Có n cách thực hiện công đoạn  A, m cách thực hiện phương án B. Khi đó công việc có thể được thực hiện bởi m.n cách.

**Ví dụ:**

- Có 3 tuyến xe buýt đi từ địa điểm A đến địa điểm B. 
- Có 5 tuyến xe buýt đi từ địa điểm B đến địa điểm C. 

⇒ Vậy có 15(=3.5) cách để đi từ địa điểm A đến địa điểm C.


##### 2. Hoán vị, Tổ hợp và Chỉnh hợp 

###### a. Hoán vị (Permutation):

**Khái niệm:**

Cho tập A gồm n phần tử (n ≥ 1). Mỗi kết quả của sự sắp xếp thứ tự n phần tử của tập A được gọi là một hoán vị của n phần tử đó. 

**Công thức:**

Số hoán vị của một tập hợp có n phần tử là P_n.

**$$P_n = n! = n(n−1)(n−2)...1$$**

**Dấu hiệu:**

- Tất cả n phần tử đều phải có mặt.

- Mỗi phần tử xuất hiện một lần.

- Có thứ tự giữa các phần tử.

**Ví dụ:**

Có bao nhiêu cách sắp xếp 4 học sinh vào băng ghế có 4 chỗ.

Số cách sắp xếp 4 học sinh vào băng ghế có 4 chỗ là: 4! = 24.


###### b. Chỉnh hợp (Arrangement):

**Khái niệm:**

Cho một tập A gồm n phần tử (n≥1). Kết quả của việc lấy k phần tử khác nhau từ n phần tử của tập A và sắp xếp chúng theo một thứ tự nào đó được gọi là một chỉnh hợp chập k của n phần tử đã cho.

**Công thức:**

Số các chỉnh hợp chập k của một tập hợp có n phần tử (1≤k≤n) là:

$$A^k_n = \frac{n!}{(n−k)!} = n(n−1)(n−2)...(n−k+1)$$

**Dấu hiệu:**

- Cần chọn k phần tử từ n phần tử, mỗi phần tử xuất hiện một lần.

- k phần tử đã cho được sắp xếp thứ tự.

**Ví dụ:**

Có bao nhiêu cách sắp xếp 4 học sinh vào băng ghế có 3 chỗ.

Số cách sắp xếp 4 học sinh vào băng ghế có 3 chỗ là: 4A3 = 24.


###### c. Tổ hợp (Combination):

**Khái niệm:**

Cho tập hợp X gồm n phần tử phân biệt (n≥1). Mỗi cách chọn ra k (n ≥ k ≥ 1) phần tử của X được gọi là một tổ hợp chập k của n phần tử.

**Công thức:**

Số các tổ hợp chập k của một tập hợp có n phần tử (1≤k≤n) là:

$$C^k_n = \frac{n!}{k!(n−k)!} = \frac{n(n−1)(n−2)...(n−k+1)}{k(k−1)(k−2)...1}$$

**Dấu hiệu:**

- Cần chọn k phần tử từ n phần tử, mỗi phần tử xuất hiện một lần.

- Không quan tâm đến thứ tự k phần tử đã chọn.

**Ví dụ:**

Có bao nhiêu cách chọn 4 quyển sách từ 10 quyển sách.

Số cách chọn 4 quyển sách từ 10 quyển sách là: 10C3 = 120.


#### IV. Xác suất


##### 1. Định nghĩa xác suất

**Khái niệm:**

Xác suất là khả năng biến cố sẽ xảy ra.

Ký hiệu:

$$P(A)$$

**Công thức:**

$$P(A) = \frac{m}{n}$$

Trong đó:

- m là số trường hợp mong muốn xảy ra

- n là toàn bộ trường hợp có thể xảy ra

**Ví dụ:**

Tung một đồng xu. Tìm xác suất tung được mặt ngửa:

Số trường hợp tung đồng xu được mặt ngửa: m = 1 - {ngửa}

Toàn bộ trường hợp có thể xảy ra: n = 2 - {ngửa, sấp}

Xác suất tung được mặt ngửa: $$ P(head) = \frac{m}{n} = \frac{1}{2} = 50 \%$$ 


##### 2. Các loại xác suất


###### a. Xác suất cổ điển (Classical Probability):

**Khái niệm:**

Xác suất cổ điển được áp dụng khi số trường hợp xảy ra hữu hạn và đồng khả năng.

**Ví dụ:**

Xác suất xuất hiện mặt ngửa khi tung một đồng xu cân đối, đồng chất là xác suất cổ điển vì:

- Số trường hợp xảy ra là hữu hạn: {ngửa, sấp}.

- Khả năng các trường hợp xảy ra là như nhau - đồng xu cân đối, đồng chất.


###### b. Xác suất thực nghiệm (Empirical Probability):

**Khái niệm:**

Xác suất thực nghiệm là xác suất được tính bằng tỉ số giữa tần số biến cố xảy ra và số phép thử. Ở xác suất thực nghiệm, mỗi phép thử đều khác nhau do tác động bên ngoài.

**Ví dụ:** 

Xác suất để cầu thủ sút penalty vào khung thành là xác suất thực nghiệm vì mỗi lần sút bóng đều khác nhau do nhiều yếu tố tác động:

- Sức khỏe cầu thủ

- Tình hình trận đấu

- Phong độ

...


###### c. Xác suất chủ quan (Subjective Probability):

**Khái niệm:**

Xác suất chủ quan là xác suất được đưa ra bởi một cá nhân nào đó dựa trên dữ liệu có liên quan hoặc kinh nghiệm của họ. Xác suất chủ quan không bao gồm công thức cụ thể nào mà chỉ phản ánh ý kiến cá nhân của người đưa ra xác suất.

**Ví dụ:** 

Xác suất tỉ lệ người thất nghiệp ở Việt Nam sẽ giảm trong năm 2020 là xác suất chủ quan vì: 

- Có quá nhiều yếu tố tác động.

- Không thể dự đoán dựa trên những dữ liệu gần đây - dữ liệu về tỷ lệ thất nghiệp trong 10 năm hoặc 100 năm gần đây.

#### V. Nguồn

Thông tin trong bài viết được tham khảo và tổng hợp từ: 

1. Lý thuyết tập hợp - Thống kê máy tính (Nguyễn Đình Thúc, Đặng Hải Vân, Lê Phong). *Sách*

2. Biến cố ngẫu nhiên và xác suất - Lý thuyết xác suất và thống kê toán (Đh Tài chính marketing). *Sách* 

3. [Hoán vị, chỉnh hợp, tổ hợp và bài tập áp dụng-Toán 11 - Hayhochoi.vn. *Website*](https://hayhochoi.vn/hoan-vi-chinh-hop-to-hop-va-bai-tap-ap-dung-toan-11.html)

4. [Phương pháp giải bài toán đếm - Toanmath.com. *Website*](https://toanmath.com/2018/06/phuong-phap-giai-bai-toan-dem.html)

5. [Types of probability - Statistics Foundations: 1 (LinkedIn Learning). *Video*](https://www.linkedin.com/learning/statistics-foundations-1/)

6. [Subjective Probability - Investopedia. *Website*](https://www.investopedia.com/terms/s/subjective_probability.asp)