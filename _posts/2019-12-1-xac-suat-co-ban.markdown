---
layout: post
title:  "Bài 2: Xác suất cơ bản"
toc: true
tags: ps
---

#### I. Mối quan hệ giữa các biến cố


##### 1. Hai biến cố độc lập

**Khái niệm:**

Hai biến cố $$A$$ và $$B$$ gọi là độc lập với nhau nếu việc xảy ra hay không xảy ra của biến cố này không làm thay đổi xác suất xảy ra của biến cố kia và ngược lại.

Hai biến cố không độc lập với nhau thì gọi là phụ thuộc.

**Ví dụ:**

Tung đồng xu 2 lần:

- $$A$$ là biến cố "Tung được mặt ngửa khi tung đồng xu lần thứ 1".

- $$B$$ là biến cố "Tung được mặt ngửa khi tung đồng xu lần thứ 2".

⇒ $$A$$ và $$B$$ là hai biến cố độc lập.


##### 2. Hai biến cố xung khắc

**Khái niệm:**

Hai biến cố $$A$$ và $$B$$ xung khắc với nhau nếu chúng không thể đồng thời xảy ra trong cùng một phép thử

$$A$$ và $$B$$ xung khắc <=> $$A.B$$ = rỗng

**Ví dụ:**

Trong một bình có 2 loại quả cầu: cầu trắng và cầu đỏ. Lấy ngẫu nhiên từ bình đó một quả cầu.

- Gọi $$A$$ là biến cố "Lấy được quả cầu trắng".

- Gọi $$B$$ là biến cố "Lấy được quả cầu đỏ".

⇒ $$A$$ và $$B$$ là hai biến cố xung khắc.

##### 3. Nhóm biến cố đầy đủ

**Khái niệm:**

Các biến cố $$A_i$$ (i=1...n) được gọi là một nhóm đầy đủ các biến cố (hay nhóm biến cố đầy đủ) nếu trong kết quả của một phép thử sẽ xảy ra một và chỉ một trong các biến cố đó.

**Ví dụ:**

Gieo một con xúc xắc 

Gọi $$A_i$$ là biến cố  "Xuất hiện mặt có $$i$$ chấm", $$i = \overline{1,6}$$

=> Các biến cố $$A_1,A_2,...A_6$$ tạo nên một nhóm biến cố đầy đủ.


##### 4. Hai biến cố đối lập

Hai biến cố $$A$$ và $$\bar{A}$$ gọi là đối lập với nhau nếu chúng tạo nên một nhóm biến cố đầy đủ.

$$A$$ và $$\bar{A}$$ đối lập <=> $$A$$ + $$\bar{A}$$ =  kgm và $$A.\bar{A} = \text{\O}$$

#### II. Các công thức cơ bản


##### 1. Xác suất có điều kiện:

**Khái niệm:**

Xác suất của biến cố $$A$$ được tính với điều kiện biến cố $$B$$ đã xảy ra gọi là xác suất của $$A$$ với điều kiện $$B$$ hoặc xác suất có điều kiện của $$A$$.

**Ký hiệu:**

$$P(A/B)$$

**Ví dụ:**

Trong một bình có 5 quả cầu trắng và 3 quả cầu đen. Lấy ngẫu nhiên lần lượt 2 quả cầu không hoàn lại.

Gọi $$A_i$$ là biến cố "Lấy được quả cầu trắng ở lần thứ $$i$$", $$i = 1,2$$.

Ta có xác suất để lấy quả cầu trắng ở lần thứ 1:


##### 2. Công thức nhân xác suất:

**Khái niệm:**

Xác suất của tích hai biến cố $$A$$ và $$B$$ bằng tích xác suất của một trong hai biến cố của một trong hai biến cố đó với xác suất có điều kiện của biến cố còn lại.

**Công thức:**

$$P(A.B) =  P(A).P(B/A) = P(B).P(A/B)$$

**Lưu ý:**

Nếu hai biến cố $$A$$ và $$B$$ độc lập thì:

$$P(A.B) = P(A).P(B)$$

**Ví dụ:**

Trong 1 bình có 5 quả cầu trắng và 3 quả cầu đen. Lấy ngẫu nhiên lần lượt 2 quả cầu. Tính xác suất để lấy được 2 quả cầu trắng. Biết rằng các quả cầu được lấy không hoàn lại.

Xác suất lấy được thứ nhất là quả cầu màu trắng: $$\frac{5}{8}$$ (5 quả cầu trắng trong số 8 quả cầu)

Xác suất lấy được thứ hai là quả cầu màu trắng: $$\frac{4}{7}$$ (1 quả cầu trắng đã bị lấy đi nên trong bình còn lại 4 quả cầu trắng trong số 7 quả cầu)

**Hệ quả:**

|----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Hệ quả 1 | - Nếu $$P(B) > 0$$ thì xác suất của biến cố $$A$$ với điều kiện biến cố $$B$$ đã xảy ra được tính bằng công thức:<br>\\[P(A/B) = \frac{P(A.B)}{P(B)}\\]<br>Nếu $$P(B) = 0$$ thì xác suất này không xác định.<br>- Tương tự nếu $$P(A) > 0$$ thì ta có:<br>\\[P(B/A) = \frac{P(A.B)}{P(A)}\\] |
| Hệ quả 2 | Nếu $$A$$ và $$B$$ độc lập thì:<br>\\[ \begin{eqnarray}P(A) &=& \frac{P(A.B)}{P(B)} \text{ khi P(B) > 0} \\\ P(B) &=& \frac{P(A.B)}{P(A)} \text{ khi P(A) > 0} \end{eqnarray} \\]|
| Hệ quả 3 | Xác suất của tích n biến cố phụ thuộc bằng tích xác suất của n biến cố đó, trong đó xác suất của mỗi biến cố tiếp sau đều được tính với điều kiện tất cả các biến cố xét trước đó đã xảy ra.<br>\\[P(A1.A2.A3..An) = P(A1).P(A2/A1).P(A3/A1.A2)...P(An/A1.A2...An−1)\\] |
| Hệ quả 4 | Xác suất của tích n biến cố độc lập toàn phần bằng tích các xác suất thành phần.<br>\\[P(\prod_{i=1}^n(A_i)) = \prod_{i=1}^n (P(A_i))\\] |

### 3. Công thức cộng xác suất

**Khái niệm:**

Xác suất của tổng hai biến cố không xung khắc bằng tổng xác suất các biến cố đó trừ đi xác suất của tích các biến cố đó.

**Công thức:**

Nếu $$A$$ và $$B$$ không xung khắc:

$$P(A+B) = P(A) + P(B) - P(A.B)$$

**Lưu ý:**

Xác suất của tổng hai biến cố xung khắc bằng tổng xác suất biến cố đó.

Nếu $$A$$ và $$B$$ xung khắc:

$$P(A+B) = P(A)+P(B)$$

**Ví dụ:**

Tung 2 con xúc xắc không đồng chất. Xác suất để có được mặt 6 chấm ở con xúc xắc thứ 1 là 20% và ở con xúc xắc thứ 2 là 10%. Tính xác suất để mặt 6 chấm xuất hiện.

- $$A_i$$ là biến cố: "Có được mặt 6 chấm ở con xúc xắc thứ $$i$$", $$i = 1,2$$.

- $$A$$ là biến cố: "Mặt 6 chấm xuất hiện".

- Ta có: $$A = A_1 + A_2$$

- Xác suất để mặt 6 chấm xuất hiện ($$A_1$$ và $$A_2$$ không xung khắc): $$P(A_1+A_2) = 0,2 + 0,1 - 0,2.0,1 = 0,28$$
